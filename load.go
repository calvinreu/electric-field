package main

import (
	"encoding/json"
	"io/ioutil"
)

func Load(filename string) ([]Particle, error) {
	var particles []Particle
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(data, &particles)
	if err != nil {
		return nil, err
	}

	for _, i := range particles {
		if i.Charge > 0 {
			i.guiInstance = window.Sprites[positiveSpriteID].NewInstance(float32(i.x), float32(i.y))
		} else {
			i.guiInstance = window.Sprites[negativeSpriteID].NewInstance(float32(i.x), float32(i.y))
		}
	}
	return particles, nil
}
