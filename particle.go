package main

import (
	"gitlab.com/calvinreu/graphic"
)

type Particle struct {
	Charge, Mass float64
	x, y         float64
	guiInstance  *graphic.Instance
}

func (particle *Particle) GetPosition() (float64, float64) {
	return particle.x, particle.y
}

func (particle *Particle) Move(x, y float64) error {
	particle.guiInstance.ChangePosition(float32(x), float32(y))

	particle.x += x
	particle.y += y

	return nil
}

func (particle *Particle) SetPosition(x, y float64) error {
	particle.guiInstance.NewPosition(float32(x), float32(y))

	particle.x = x
	particle.y = y

	return nil
}
