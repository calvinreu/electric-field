package main

const (
	GraphicSpriteConfig = "ressources/sprite.json"
	GraphicWindowConfig = "ressources/window.json"
	defaultSaveLoadFile = "ressources/default.json"
	defaultRunMode      = "run"
	runRunMode          = "run"
	createRunMode       = "create"
	negativeSpriteID    = 0
	positiveSpriteID    = 1
)
