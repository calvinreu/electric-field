package main

import (
	"flag"
	"fmt"

	"gitlab.com/calvinreu/graphic"
)

var DEBUG bool
var window graphic.Window
var particles []Particle

func main() {
	var err error
	var mode, filename string
	var windowConfig graphic.Config
	flag.BoolVar(&DEBUG, "DEBUG", false, "Commandline info")
	flag.StringVar(&mode, "mode", defaultRunMode, "Mode to run options: run, create")
	flag.StringVar(&filename, "filename", defaultSaveLoadFile, "filename of save or Loadfile")
	flag.Parse()

	windowConfig.Load(GraphicWindowConfig, GraphicSpriteConfig)
	window.Init(windowConfig.Window)

	switch mode {
	case runRunMode:
		particles, err = Load(filename)
		if err != nil {
			fmt.Println(err)
			return
		}
		Run()
	case createRunMode:
		particles = make([]Particle, 0)
		Create()
	default:
		fmt.Println("unkown mode")
	}
}
